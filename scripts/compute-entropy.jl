using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

include(srcdir("Preamble.jl"))
include(srcdir("Kagome.jl"))

using Logging
using Printf
using LinearAlgebra
using DataStructures
using ArgParse
using JSON
using BitIntegers

using LatticeTools
using QuantumHamiltonian
using Arpack


using PyCall
npl = pyimport("numpy.linalg")


function compute_kagome(
        Jx::Real, Jy::Real, Jz ::Real, J2::Real,
        shape::AbstractMatrix{<:Integer},
        Szs::AbstractVector{<:Real},
        tsym_irrep_indices::AbstractVector{<:Integer},
        psym_irrep_indices::AbstractVector{<:Integer},
        psym_irrep_components::AbstractVector{<:Integer},
        max_dense::Integer, 
        nev::Integer,
        force::Bool)
    log("lattice shape: $shape")
    log("Jx: $Jx")
    log("Jy: $Jy")
    log("Jz: $Jz")
    log("J2: $J2")
    log("Sz: $Szs")

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]

    # == Prepare Hilbert space ==
    (unitcell, lattice, ssymbed, nnbonds, nnnbonds, nntris) = make_kagome_lattice(shape);

    n_sites = numorbital(lattice.supercell)
    log("Number of sites: $n_sites")

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end
        
    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    # == Prepare Hamiltonian ==
    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    jx2, jy2, jz2 = NullOperator(), NullOperator(), NullOperator()

    for ((irow, icol), _, bondsign) in nnbonds
        jx += pauli(irow, :x) * pauli(icol, :x) * bondsign
        jy += pauli(irow, :y) * pauli(icol, :y) * bondsign
        jz += pauli(irow, :z) * pauli(icol, :z) * bondsign
    end

    for ((irow, icol), _, bondsign) in nnnbonds
        jx2 += pauli(irow, :x) * pauli(icol, :x) * bondsign
        jy2 += pauli(irow, :y) * pauli(icol, :y) * bondsign
        jz2 += pauli(irow, :z) * pauli(icol, :z) * bondsign
    end

    hamiltonian = simplify( (Jx*jx + Jy*jy + Jz*jz) + J2*(Jx*jx2 + Jy*jy2 + Jz*jz2) ) * 0.25
    @assert isinvariant(hs, ssymbed, hamiltonian)
    log("Hamiltonian is invariant under $(symmetry_name(ssymbed.normal.symmetry)) ⋊ $(symmetry_name(ssymbed.rest.symmetry))")

    sx = 0.5 * sum(pauli(i, :x) for i in 1:n_sites)
    sy = 0.5 * sum(pauli(i, :y) for i in 1:n_sites)
    sz = 0.5 * sum(pauli(i, :z) for i in 1:n_sites)
    spin_squared = simplify(sx*sx + sy*sy + sz*sz)

    log("Quantum numbers: $(quantum_number_sectors(hs))")

    if isempty(Szs)
        qns = quantum_number_sectors(hs)
    else
        qns = [(round(Int, x*2), ) for x in Szs]
    end

    log("Target quantum numbers: $(qns)")

    for (qn,) in qns
        Sz = qn/2
        log("- spin_z: $(Sz)")
        
        hss = HilbertSpaceSector(hs, qn)
        hssr = represent(hss, BR)

        for ssic in get_irrep_components(ssymbed)
            if !isempty(tsym_irrep_indices) && (ssic.normal.irrep_index ∉ tsym_irrep_indices)
                continue
            elseif !isempty(psym_irrep_indices) && (ssic.rest.irrep_index ∉ psym_irrep_indices)
                continue
            elseif !isempty(psym_irrep_components) && (ssic.rest.irrep_component ∉ psym_irrep_components)
                continue
            end

            k = fractional_momentum(ssymbed, ssic.normal.irrep_index)
            rhssr = symmetry_reduce(hssr, ssic)
            log("  - trans symmetry irrep index : $(ssic.normal.irrep_index)")
            log("    momentum (in units of 2π)  : $(lattice.unitcell.reducedreciprocallatticevectors * k)")
            log("    little point symmetry      : $(symmetry(ssic.rest.symmetry).hermann_mauguin)")
            log("    point symmetry irrep index : $(ssic.rest.irrep_index)")
            log("    point symmetry irrep compo : $(ssic.rest.irrep_component)")
            log("    Hilbert space dimension    : $(dimension(rhssr))")

            hilbert_space_dimension = dimension(rhssr)

            hilbert_space_dimension == 0 && continue

            parameter = Dict{Symbol, Any}(
                              :shape=>"($n11,$n21)x($n12,$n22)",
                              :Jx=>@sprintf("%.1f", Jx),
                              :Jy=>@sprintf("%.1f", Jy),
                              :Jz=>@sprintf("%.1f", Jz),
                              :J2=>@sprintf("%.1f", J2))
            parameter[:Sz] = @sprintf("%.1f", Sz)
            parameter[:tii] = ssic.normal.irrep_index
            parameter[:pii] = ssic.rest.irrep_index
            parameter[:pic] = ssic.rest.irrep_component

            if hilbert_space_dimension > max_dense
                matrix_type = "sparse"
            else
                matrix_type = "dense"
            end
            log("    matrix type                : $matrix_type")

            output_filename = savename("spectrum-$matrix_type-measure", parameter, "json")
            output_filepath = datadir(joinpath("($n11,$n21)x($n12,$n22)", output_filename))
            if ispath(output_filepath)
                log("File $output_filepath exists.")
                if force
                    log("Overwriting.")
                else
                    log("Skipping.")
                    continue
                end
            end
            log("Will save results to $(output_filepath)")

            hamiltonian_rep = represent(rhssr, hamiltonian)
            spin_squared_rep = represent(rhssr, spin_squared)

            log("Computing eigenspectrum")

            if matrix_type == "sparse"
                eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=nev, which=:SR)
                eigenvalues = real.(eigenvalues)
            else  # if matrix_type == "dense"
                hamiltonian_dense = Matrix(hamiltonian_rep)
                eigenvalues, eigenvectors = npl.eigh(hamiltonian_dense)
            end
            function enlarge(vec_small::AbstractVector{T}) where {T<:Number}
                vec_middle = symmetry_unreduce(rhssr, vec_small)
                #out = zeros(T, 2^numorbital(lattice.supercell))
                @assert length(vec_middle) == length(hssr.basis_list)
                out = []
                for (bvec, ampl) in zip(hssr.basis_list, vec_middle)
                    if !isapprox(ampl, zero(ampl); atol=Base.rtoldefault(Float64))
                        push!(out, [bvec, real(ampl), imag(ampl)])
                    end
                end
                return out
            end

            log("Computing large eigenvectors")
            eigenvectors_large = Vector{Any}(undef, length(eigenvalues))
            Threads.@threads for i_eigen in eachindex(eigenvalues)
                eigenvectors_large[i_eigen] = enlarge(eigenvectors[:, i_eigen])
            end

            spin_two_list = zeros(Float64, length(eigenvalues))
            spin_four_list = zeros(Float64, length(eigenvalues))

            log("Computing observables")

            Threads.@threads for i_eigen in eachindex(eigenvalues)
                phi = eigenvectors[:, i_eigen]
                s2phi = spin_squared_rep * phi
                s4phi = spin_squared_rep * s2phi
                s2 = real(dot(phi, s2phi))
                s4 = real(dot(phi, s4phi))

                spin_two_list[i_eigen] = s2
                spin_four_list[i_eigen] = s4
            end # for i_eigen

            output_dict = OrderedDict(
                    "parameter" => OrderedDict("J2" => J2, "Jx" => Jx, "Jy" => Jy, "Jz" => Jz, "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]] ),
                    "sector" => OrderedDict(
                            "Sz" => Sz,
                            "tsym_irrep_index" => ssic.normal.irrep_index,
                            "psym_irrep_index" => ssic.rest.irrep_index,
                            "psym_irrep_component" => ssic.rest.irrep_component,
                            "matrix_type" => matrix_type,
                            "dimension" => hilbert_space_dimension,
                        ),
                    "eigenvalue" => eigenvalues,
                    "eigenvector" => eigenvectors_large,
                    #OrderedDict("real" => real.(eigenvectors_large),
                    #            "imag" => imag.(eigenvectors_large)),
                    "spin_two" => spin_two_list,
                    "spin_four" => spin_four_list,
                )                     
            log("Saving to $(output_filepath)")

            mkpath(dirname(output_filepath); mode=0o755)
            open(output_filepath, "w") do fp
                JSON.print(fp, output_dict)
            end
            log("Successfully saved to $(output_filepath)")
        end # for ssic
    end
end


function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--shape"
        arg_type = Int
        nargs = 4
        required = true
        help = "shape of the cluster in units of lattice vectors (a,b) x (c,d)"
        "--Jx"
        arg_type = Float64
        nargs = '*'
        default = [1.0]
        help = "values of Jx"
        "--Jy"
        arg_type = Float64
        nargs = '*'
        default = [1.0]
        help = "values of Jy"
        "--Jz"
        arg_type = Float64
        nargs = '+'
        help = "values of Jz"
        "--J2"
        arg_type = Float64
        nargs = '*'
        default = [0.0]
        help = "values of J2"
        "--Sz"
        arg_type = Float64
        nargs = '*'
        help = "values of Sz to consider"
        "--tii"
        arg_type = Int64
        nargs = '*'
        help = "translation symmetry irrep index (momentum index)"
        "--pii"
        arg_type = Int64
        nargs = '*'
        help = "(little) point symmetry irrep index"
        "--pic"
        arg_type = Int64
        nargs = '*'
        help = "(little) point symmetry irrep component"
        "--max-dense"
        arg_type = Int64
        default = 20000
        help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
        arg_type = Int
        default = 300
        help = "number of eigenvalues to compute when using sparse"
        "--force", "-f"
        help = "force run (overwrite)"
        action = :store_true
        "--debug"
        help = "Debug"
        action = :store_true
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    ss = parsed_args["shape"]
    shape = [ss[1] ss[3]; ss[2] ss[4]]

    J2s = parsed_args["J2"]
    Jxs = parsed_args["Jx"]
    Jys = parsed_args["Jy"]
    Jzs = parsed_args["Jz"]

    Sz = parsed_args["Sz"]
    tii = parsed_args["tii"]
    pii = parsed_args["pii"]
    pic = parsed_args["pic"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]

    force = parsed_args["force"]

    for (J2, Jx, Jy, Jz) in Iterators.product(J2s, Jxs, Jys, Jzs)
        compute_kagome(Jx, Jy, Jz, J2, shape, Sz, tii, pii, pic, max_dense, nev, force)
    end
end

main()
